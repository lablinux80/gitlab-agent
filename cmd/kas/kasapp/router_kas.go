package kasapp

import (
	"context"
	"errors"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/logz"
	"go.opentelemetry.io/otel/attribute"
	otelcodes "go.opentelemetry.io/otel/codes"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	kasRoutingStatusLabelName attribute.Key = "status"
)

var (
	kasRoutingStatusSuccessAttrSet = attribute.NewSet(kasRoutingStatusLabelName.String("success"))
	kasRoutingStatusAbortedAttrSet = attribute.NewSet(kasRoutingStatusLabelName.String("aborted"))
)

// RouteToKasStreamHandler is a gRPC handler that routes the request to another kas instance.
// Must return a gRPC status-compatible error.
func (r *router) RouteToKasStreamHandler(srv interface{}, stream grpc.ServerStream) error {
	// 0. boilerplate
	ctx := stream.Context()
	md, _ := metadata.FromIncomingContext(ctx)
	agentId, err := agentIdFromMeta(md)
	if err != nil {
		return err
	}
	rpcApi := modserver.RpcApiFromContext(ctx)

	// 1. find a ready, suitable tunnel
	rt, err := r.findReadyTunnel(ctx, rpcApi, md, agentId)
	if err != nil {
		return err
	}
	defer rt.Done()

	// 2. start streaming via the found tunnel
	f := kasStreamForwarder{
		log:               rpcApi.Log().With(logz.AgentId(agentId), logz.KasUrl(rt.kasUrl)),
		rpcApi:            rpcApi,
		gatewayKasVisitor: r.gatewayKasVisitor,
	}
	return f.ForwardStream(rt.kasStream, stream)
}

func (r *router) findReadyTunnel(ctx context.Context, rpcApi modserver.RpcApi, md metadata.MD, agentId int64) (readyTunnel, error) {
	startRouting := time.Now()
	findCtx, span := r.tracer.Start(ctx, "router.findReadyTunnel", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()
	log := rpcApi.Log().With(logz.AgentId(agentId))
	tf := newTunnelFinder(
		log,
		r.kasPool,
		r.tunnelQuerier,
		rpcApi,
		grpc.ServerTransportStreamFromContext(ctx).Method(),
		r.ownPrivateApiUrl,
		agentId,
		metadata.NewOutgoingContext(ctx, md),
		r.pollConfig,
		r.gatewayKasVisitor,
		r.tryNewKasInterval,
	)
	findCtx, findCancel := context.WithTimeout(findCtx, r.tunnelFindTimeout)
	defer findCancel()

	rt, err := tf.Find(findCtx)
	if err != nil {
		switch { // Order is important here.
		case ctx.Err() != nil: // Incoming stream cancelled.
			r.kasRoutingDuration.Record( // nolint: contextcheck
				context.Background(),
				float64(time.Since(startRouting))/float64(time.Second),
				otelmetric.WithAttributeSet(kasRoutingStatusAbortedAttrSet),
			)
			span.SetStatus(otelcodes.Error, "Aborted")
			span.RecordError(ctx.Err())
			return readyTunnel{}, grpctool.StatusErrorFromContext(ctx, "RouteToKasStreamHandler request aborted")
		case findCtx.Err() != nil: // Find tunnel timed out.
			r.kasRoutingTimeout.Add(context.Background(), 1) // nolint: contextcheck
			findCtxErr := findCtx.Err()
			span.SetStatus(otelcodes.Error, "Timed out")
			span.RecordError(findCtxErr)
			rpcApi.HandleProcessingError(log, agentId, "Agent connection not found", errors.New(findCtxErr.Error()))
			return readyTunnel{}, status.Error(codes.DeadlineExceeded, "Agent connection not found. Is agent up to date and connected?")
		default: // This should never happen, but let's handle a non-ctx error for completeness and future-proofing.
			span.SetStatus(otelcodes.Error, "Failed")
			span.RecordError(err)
			return readyTunnel{}, status.Errorf(codes.Unavailable, "Find tunnel failed: %v", err)
		}
	}
	r.kasRoutingDuration.Record( // nolint: contextcheck
		context.Background(),
		float64(time.Since(startRouting))/float64(time.Second),
		otelmetric.WithAttributeSet(kasRoutingStatusSuccessAttrSet),
	)
	span.SetStatus(otelcodes.Ok, "")
	return rt, nil
}
