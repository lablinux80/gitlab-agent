package tool

// TransformMap transforms map of type IN into a map of type OUT.
// The main point of this function is to optimize memory usage. It allocates a single slice for
// the elements in values of the map rather than a slice per key.
func TransformMap[K comparable, V1 any, V2 any, IN ~map[K]V1, OUT ~map[K]V2, E any](
	in IN,
	v1ToElem func(V1) []E,
	elemToV2 func([]E) V2) OUT {

	if len(in) == 0 {
		return nil
	}

	result := make(OUT, len(in))
	valsLen := 0
	for _, v := range in {
		valsLen += len(v1ToElem(v))
	}
	vals := make([]E, 0, valsLen) // allocate backing array for all elements in one go
	for k, v := range in {
		vals = append(vals, v1ToElem(v)...)
		// set capacity to length to protect against potential append overwriting next value
		lk := len(vals)
		result[k] = elemToV2(vals[:lk:lk])
		vals = vals[lk:]
	}
	return result
}
