package mock_otel

//go:generate mockgen.sh -destination "otel.go" -package "mock_otel" "go.opentelemetry.io/otel/metric" "Meter,Int64UpDownCounter"
//go:generate ./apply_patch.sh
