package grpctool

import (
	"context"
	"strings"
	"sync"

	"go.opentelemetry.io/otel/attribute"
	otelmetric "go.opentelemetry.io/otel/metric"
	"google.golang.org/grpc/stats"
)

const (
	grpcServiceAttr attribute.Key = "grpc_service"
	grpcMethodAttr  attribute.Key = "grpc_method"
	namespace                     = "grpc_"
)

type rpcTagCtxKey struct{}

type RequestsInFlightStatsHandler struct {
	mu      sync.Mutex               // protects attrs only
	attrs   map[string]attribute.Set // full method name -> attribute set
	counter otelmetric.Int64UpDownCounter
}

func NewClientRequestsInFlightStatsHandler(m otelmetric.Meter) (*RequestsInFlightStatsHandler, error) {
	return NewRequestsInFlightStatsHandler("client", m)
}

func NewServerRequestsInFlightStatsHandler(m otelmetric.Meter) (*RequestsInFlightStatsHandler, error) {
	return NewRequestsInFlightStatsHandler("server", m)
}

func NewRequestsInFlightStatsHandler(sub string, m otelmetric.Meter) (*RequestsInFlightStatsHandler, error) {
	c, err := m.Int64UpDownCounter(namespace+sub+"_requests_in_flight",
		otelmetric.WithDescription("Number of requests in flight"),
	)
	if err != nil {
		return nil, err
	}
	return &RequestsInFlightStatsHandler{
		counter: c,
	}, nil
}

func (h *RequestsInFlightStatsHandler) TagRPC(ctx context.Context, inf *stats.RPCTagInfo) context.Context {
	h.mu.Lock()
	defer h.mu.Unlock()
	attrs, ok := h.attrs[inf.FullMethodName]
	if !ok {
		service, method := parseMethod(inf.FullMethodName)
		attrs = attribute.NewSet(grpcServiceAttr.String(service), grpcMethodAttr.String(method))
	}
	return context.WithValue(ctx, rpcTagCtxKey{}, attrs)
}

func (h *RequestsInFlightStatsHandler) TagConn(ctx context.Context, _ *stats.ConnTagInfo) context.Context {
	return ctx
}

func (h *RequestsInFlightStatsHandler) HandleConn(_ context.Context, _ stats.ConnStats) {
}

func (h *RequestsInFlightStatsHandler) HandleRPC(ctx context.Context, stat stats.RPCStats) {
	switch stat.(type) {
	case *stats.Begin:
		attrs := ctx.Value(rpcTagCtxKey{}).(attribute.Set)
		h.counter.Add(context.Background(), 1, otelmetric.WithAttributeSet(attrs)) // nolint:contextcheck
	case *stats.End:
		attrs := ctx.Value(rpcTagCtxKey{}).(attribute.Set)
		h.counter.Add(context.Background(), -1, otelmetric.WithAttributeSet(attrs)) // nolint:contextcheck
	}
}

func parseMethod(name string) (string, string) {
	if !strings.HasPrefix(name, "/") {
		return "unknown", "unknown"
	}
	name = name[1:]

	pos := strings.LastIndex(name, "/")
	if pos < 0 {
		return "unknown", "unknown"
	}
	return name[:pos], name[pos+1:]
}
