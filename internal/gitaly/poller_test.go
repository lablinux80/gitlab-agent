package gitaly

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/gitaly/vendored/gitalypb"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/matcher"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_gitaly"
	"go.uber.org/mock/gomock"
)

const (
	revision1 = "507ebc6de9bcac25628aa7afd52802a91a0685d8"
	revision2 = "28aa7afd52802a91a0685d8507ebc6de9bcac256"
)

var (
	_ PollerInterface = &Poller{}
)

func TestPoller_HappyPath(t *testing.T) {
	tests := []struct {
		name                string
		lastProcessedCommit string
		expectedInfoCommit  string
		expectedInfoUpdate  bool
	}{
		{
			name:                "same commit",
			lastProcessedCommit: revision1,
			expectedInfoCommit:  revision1,
			expectedInfoUpdate:  false,
		},
		{
			name:                "no commit",
			lastProcessedCommit: "",
			expectedInfoCommit:  revision1,
			expectedInfoUpdate:  true,
		},
		{
			name:                "another commit",
			lastProcessedCommit: revision2,
			expectedInfoCommit:  revision1,
			expectedInfoUpdate:  true,
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			r := repo()
			client := mock_gitaly.NewMockCommitServiceClient(ctrl)
			features := map[string]string{
				"f1": "true",
			}
			client.EXPECT().
				FindCommit(matcher.GrpcOutgoingCtx(features), matcher.ProtoEq(nil, &gitalypb.FindCommitRequest{
					Repository: r,
					Revision:   []byte(DefaultBranch),
				}), gomock.Any()).
				Return(&gitalypb.FindCommitResponse{
					Commit: &gitalypb.GitCommit{
						Id: revision1,
					},
				}, nil)
			p := Poller{
				Client:   client,
				Features: features,
			}
			pollInfo, err := p.Poll(context.Background(), r, tc.lastProcessedCommit, DefaultBranch)
			require.NoError(t, err)
			assert.Equal(t, tc.expectedInfoUpdate, pollInfo.UpdateAvailable)
			assert.Equal(t, tc.expectedInfoCommit, pollInfo.CommitId)
		})
	}
}

func TestPoller_RefNotFound(t *testing.T) {
	ctrl := gomock.NewController(t)
	r := repo()
	client := mock_gitaly.NewMockCommitServiceClient(ctrl)
	client.EXPECT().
		FindCommit(gomock.Any(), matcher.ProtoEq(nil, &gitalypb.FindCommitRequest{
			Repository: r,
			Revision:   []byte(DefaultBranch),
		}), gomock.Any()).
		Return(&gitalypb.FindCommitResponse{}, nil)
	p := Poller{
		Client: client,
	}
	pollInfo, err := p.Poll(context.Background(), r, "", DefaultBranch)
	require.NoError(t, err)
	assert.True(t, pollInfo.RefNotFound)
}

func repo() *gitalypb.Repository {
	return &gitalypb.Repository{
		StorageName:        "StorageName",
		RelativePath:       "RelativePath",
		GitObjectDirectory: "GitObjectDirectory",
		GlRepository:       "GlRepository",
		GlProjectPath:      "GlProjectPath",
	}
}
