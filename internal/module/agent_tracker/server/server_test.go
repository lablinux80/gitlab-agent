package server

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_tracker/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/pkg/entity"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap/zaptest"
)

func TestServer_GetConnectedAgents_HappyPathWithProjectId(t *testing.T) {
	mockRpcApi, mockAgentQuerier, s, ctx := setupServer(t)

	req := &rpc.GetConnectedAgentsRequest{
		Request: &rpc.GetConnectedAgentsRequest_ProjectId{
			ProjectId: 123,
		},
	}

	mockRpcApi.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	projectId := req.Request.(*rpc.GetConnectedAgentsRequest_ProjectId).ProjectId
	mockAgentQuerier.EXPECT().
		GetConnectionsByProjectId(ctx, projectId, gomock.Any()).
		Do(func(ctx context.Context, projectId int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					Version:      "v1.0.0",
					CommitId:     "commit-id",
					PodNamespace: "agentk-pod-namespace",
					PodName:      "agentk-pod-name",
				},
				ConnectionId: 123123123,
				AgentId:      123123,
				ProjectId:    projectId,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgents(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 1)
	assert.EqualValues(t, 123123123, resp.Agents[0].ConnectionId)
}

func TestServer_GetConnectedAgents_HappyPathWithAgentId(t *testing.T) {
	mockRpcApi, mockAgentQuerier, s, ctx := setupServer(t)

	req := &rpc.GetConnectedAgentsRequest{
		Request: &rpc.GetConnectedAgentsRequest_AgentId{
			AgentId: 123,
		},
	}

	mockRpcApi.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	agentId := req.Request.(*rpc.GetConnectedAgentsRequest_AgentId).AgentId
	mockAgentQuerier.EXPECT().
		GetConnectionsByAgentId(ctx, agentId, gomock.Any()).
		Do(func(ctx context.Context, projectId int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      agentId,
				ProjectId:    123123,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgents(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 1)
	assert.EqualValues(t, 123123123, resp.Agents[0].ConnectionId)
}

func TestServer_GetConnectedAgentsByProjectIds(t *testing.T) {
	mockRpcApi, mockAgentQuerier, s, ctx := setupServer(t)

	projectId1 := int64(123)
	projectId2 := int64(456)
	req := &rpc.GetConnectedAgentsByProjectIdsRequest{
		ProjectIds: []int64{projectId1, projectId2},
	}

	mockRpcApi.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	mockAgentQuerier.EXPECT().
		GetConnectionsByProjectId(ctx, projectId1, gomock.Any()).
		Do(func(ctx context.Context, projectId int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      123123,
				ProjectId:    projectId1,
			})
			require.NoError(t, err)
			return err
		})
	mockAgentQuerier.EXPECT().
		GetConnectionsByProjectId(ctx, projectId2, gomock.Any()).
		Do(func(ctx context.Context, projectId int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      456456,
				ProjectId:    projectId2,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByProjectIds(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, 123123, resp.Agents[0].AgentId)
	assert.EqualValues(t, 456456, resp.Agents[1].AgentId)
}

func TestServer_GetConnectedAgentsByAgentIds(t *testing.T) {
	mockRpcApi, mockAgentQuerier, s, ctx := setupServer(t)

	agentId1 := int64(123)
	agentId2 := int64(456)
	req := &rpc.GetConnectedAgentsByAgentIdsRequest{
		AgentIds: []int64{agentId1, agentId2},
	}

	mockRpcApi.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	mockAgentQuerier.EXPECT().
		GetConnectionsByAgentId(ctx, agentId1, gomock.Any()).
		Do(func(ctx context.Context, projectId int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      agentId1,
				ProjectId:    123123,
			})
			require.NoError(t, err)
			return err
		})
	mockAgentQuerier.EXPECT().
		GetConnectionsByAgentId(ctx, agentId2, gomock.Any()).
		Do(func(ctx context.Context, projectId int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      agentId2,
				ProjectId:    456456,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByAgentIds(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, agentId1, resp.Agents[0].AgentId)
	assert.EqualValues(t, agentId2, resp.Agents[1].AgentId)
}

func setupServer(t *testing.T) (*mock_modserver.MockAgentRpcApi, *mock_agent_tracker.MockQuerier, *server, context.Context) {
	ctrl := gomock.NewController(t)

	mockRpcApi := mock_modserver.NewMockAgentRpcApi(ctrl)
	mockAgentQuerier := mock_agent_tracker.NewMockQuerier(ctrl)

	s := &server{
		agentQuerier: mockAgentQuerier,
	}

	ctx := modserver.InjectRpcApi(context.Background(), mockRpcApi)

	return mockRpcApi, mockAgentQuerier, s, ctx
}
