package agent

//goland:noinspection GoSnakeCaseUsage
import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sync/atomic"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/remote_development/agent/k8s"
	rdutil "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/remote_development/agent/util"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/ioz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/retry"
	"go.uber.org/zap"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
)

type MessageType string
type WorkspaceUpdateType string
type TerminationProgress string
type ErrorType string

const (
	WorkspaceUpdateTypePartial WorkspaceUpdateType = "partial"
	WorkspaceUpdateTypeFull    WorkspaceUpdateType = "full"

	WorkspaceStateTerminated string = "Terminated"
	WorkspaceStateError      string = "Error"

	TerminationProgressTerminating TerminationProgress = "Terminating"
	TerminationProgressTerminated  TerminationProgress = "Terminated"

	ErrorTypeApplier    ErrorType = "applier"
	ErrorTypeKubernetes ErrorType = "kubernetes"
)

// reconciler is equipped to and responsible for carrying
// one cycle of reconciliation when reconciler.Run() is invoked
type reconciler struct {
	log          *zap.Logger
	agentId      int64
	api          modagent.Api
	pollConfig   retry.PollConfigFactory
	stateTracker *persistedStateTracker
	informer     informer
	k8sClient    k8s.Client

	// This is used to determine whether the reconciliation cycle corresponds to
	// a full or partial sync. When a reconciler runs for the first time, this will be false
	// indicating a full sync. After the full sync successfully completes, this will be
	// indicating partial sync for subsequent cycles for the same reconciler
	hasFullSyncRunBefore bool

	// terminationTracker tracks all workspaces for whom termination has been initiated or
	// they have been terminated.  These workspaces are evicted from the tracker once a workspace
	// has been completely removed from the cluster and the corresponding termination progress i.e
	// Terminated has been successfully reported to the
	terminationTracker terminationTracker

	// errDetailsTracker tracks the asynchronous error details received and serves
	// as the single source of truth when reporting these to Rails in subsequent reconciliation cycles
	errDetailsTracker *errorDetailsTracker

	// versionCounter is an atomic counter that creates monotonically increasing values for use
	// as versions by the errDetailsTracker. It is monotonically increasing in order to determine
	// if one version is older than another and thereby only watch for errors corresponding to the
	// latest operation
	versionCounter atomic.Uint64
}

// TODO: revisit all request and response types, and make more strongly typed if possible
// issue: https://gitlab.com/gitlab-org/gitlab/-/issues/396882

type WorkspaceAgentInfo struct {
	Name                    string                 `json:"name"`
	Namespace               string                 `json:"namespace,omitempty"`
	LatestK8sDeploymentInfo map[string]interface{} `json:"latest_k8s_deployment_info,omitempty"`
	TerminationProgress     TerminationProgress    `json:"termination_progress,omitempty"`
	ErrorDetails            *ErrorDetails          `json:"error_details,omitempty"`
}

type ErrorDetails struct {
	ErrorType    ErrorType `json:"error_type,omitempty"`
	ErrorMessage string    `json:"error_message,omitempty"`
}

func newErrorDetails(errorType ErrorType, errorMessage string) *ErrorDetails {
	return &ErrorDetails{ErrorType: errorType, ErrorMessage: errorMessage}
}

type RequestPayload struct {
	UpdateType          WorkspaceUpdateType  `json:"update_type"`
	WorkspaceAgentInfos []WorkspaceAgentInfo `json:"workspace_agent_infos"`
}

type WorkspaceRailsInfo struct {
	Name                      string `json:"name"`
	Namespace                 string `json:"namespace"`
	DeploymentResourceVersion string `json:"deployment_resource_version,omitempty"`
	ActualState               string `json:"actual_state,omitempty"`
	DesiredState              string `json:"desired_state,omitempty"`
	ConfigToApply             string `json:"config_to_apply,omitempty"`
}

type ResponsePayload struct {
	WorkspaceRailsInfos []*WorkspaceRailsInfo `json:"workspace_rails_infos"`
}

func (r *reconciler) Run(ctx context.Context) error {
	r.log.Debug("Running reconciliation loop")
	defer r.log.Debug("Reconciliation loop ended")

	errDetailsSnapshot := r.errDetailsTracker.createSnapshot()

	// Load and the info on the workspaces from the informer. Skip it if the persisted state in
	// rails is already up-to-date for the workspace
	workspaceAgentInfos := r.generateWorkspaceAgentInfos(errDetailsSnapshot)

	updateType := WorkspaceUpdateTypePartial
	if !r.hasFullSyncRunBefore {
		updateType = WorkspaceUpdateTypeFull
	}

	// Submit the workspace update request to the Rails API. Sends the latest AgentInfos in the request,
	// and receives the latest RailsInfos in the response.
	workspaceRailsInfos, err := r.performWorkspaceUpdateRequestToRailsApi(ctx, updateType, workspaceAgentInfos)
	if err != nil {
		return err
	}

	// Workspace update request was completed successfully, now process any RailsInfos received in the response
	for _, workspaceRailsInfo := range workspaceRailsInfos {
		// TODO: discuss whether/how to deal with errors returned by rails when processing individual workspaces
		//  for ex. there may be a case where no workspace is found for a given combination of workspace name &
		//  workspace
		// issue: https://gitlab.com/gitlab-org/gitlab/-/issues/409807

		// increment version to guarantee ordering
		version := r.versionCounter.Add(1)

		errDetailsCh := r.applyWorkspaceChanges(ctx, workspaceRailsInfo, errDetailsSnapshot)
		r.errDetailsTracker.watchForLatestErrors(ctx, workspaceRailsInfo.Name, workspaceRailsInfo.Namespace, version, errDetailsCh)
	}

	r.hasFullSyncRunBefore = true
	return nil
}

func (r *reconciler) applyWorkspaceChanges(ctx context.Context, workspaceRailsInfo *WorkspaceRailsInfo, errorDetailsSnapshot map[errorTrackerKey]operationState) <-chan *ErrorDetails {
	r.stateTracker.recordVersion(workspaceRailsInfo)

	name := workspaceRailsInfo.Name
	namespace := workspaceRailsInfo.Namespace

	r.evictReportedErrors(name, namespace, errorDetailsSnapshot)

	// When desired state is Terminated, trigger workspace deletion and exit early
	// to avoid processing/applying the workspace config
	if workspaceRailsInfo.DesiredState == WorkspaceStateTerminated {
		// Handle Terminated state (delete the namespace and workspace) and return
		err := r.handleDesiredStateIsTerminated(ctx, name, namespace, workspaceRailsInfo.ActualState)
		if err != nil {
			errMsg := fmt.Sprintf("error when handling terminated state for workspace %s: %s", name, err.Error())
			return rdutil.ToAsync[*ErrorDetails](newErrorDetails(ErrorTypeKubernetes, errMsg))
		}
		// we don't want to continue by creating namespace if we just terminated the workspace
		return rdutil.ToAsync[*ErrorDetails](nil)
	}

	// Desired state is not Terminated, so continue to handle workspace creation and config apply if needed
	namespaceExists, err := r.k8sClient.NamespaceExists(ctx, namespace)
	if err != nil {
		errMsg := fmt.Sprintf("error when checking if namespace %s exists: %s", namespace, err.Error())
		return rdutil.ToAsync[*ErrorDetails](newErrorDetails(ErrorTypeKubernetes, errMsg))
	}

	// Create namespace if it doesn't exist already
	if !namespaceExists {
		err = r.k8sClient.CreateNamespace(ctx, namespace)
		if err != nil && !k8serrors.IsAlreadyExists(err) {
			errMsg := fmt.Sprintf("failed to create a namespace %s: %s", namespace, err.Error())
			return rdutil.ToAsync[*ErrorDetails](newErrorDetails(ErrorTypeKubernetes, errMsg))
		}
	}

	// Apply workspace config if one was provided in the workspaceRailsInfo
	if workspaceRailsInfo.ConfigToApply != "" {
		errCh := r.k8sClient.Apply(ctx, workspaceRailsInfo.ConfigToApply)
		return rdutil.RunWithAsyncResult[*ErrorDetails](func(outCh chan<- *ErrorDetails) {
			for e := range errCh {
				if e != nil {
					outCh <- newErrorDetails(ErrorTypeApplier, e.Error())
				}
			}
		})
	}

	return rdutil.ToAsync[*ErrorDetails](nil)
}

func (r *reconciler) evictReportedErrors(name string, namespace string, errorDetailsSnapshot map[errorTrackerKey]operationState) {
	key := errorTrackerKey{
		name:      name,
		namespace: namespace,
	}

	// If an error was reported to rails successfully, it can now be safely cleaned up from the tracker.
	// we can do a cleanup if the version in the tracker matches the version of
	// error sent to Rails
	if state, hasEntry := errorDetailsSnapshot[key]; hasEntry && state.errorDetails != nil {
		r.errDetailsTracker.deleteErrorIfVersion(name, namespace, state.version)
	}
}

func (r *reconciler) generateWorkspaceAgentInfos(errorDetailsSnapshot map[errorTrackerKey]operationState) []WorkspaceAgentInfo {
	parsedWorkspaces := r.informer.List()

	// unterminatedWorkspaces is a set of all workspaces in the cluster returned by the informer
	// it is compared with the workspaces in the terminationTracker (considered "Terminating") to determine
	// which ones have been removed from the cluster and can be deemed "Terminated"
	unterminatedWorkspaces := make(map[string]struct{})
	for _, workspace := range parsedWorkspaces {
		// any workspace returned by the informer is deemed as having not been terminated irrespective of its status
		// workspaces that have been terminated completely will be absent from this set
		unterminatedWorkspaces[workspace.Name] = struct{}{}
	}

	workspaceInfos := r.collectInfoForUnpersistedWorkspaces(parsedWorkspaces)
	r.enrichRailsPayloadWithWorkspaceTerminationProgress(workspaceInfos, unterminatedWorkspaces)
	r.enrichRailsPayloadWithErrorDetails(workspaceInfos, errorDetailsSnapshot)

	// "result" is constructed by taking the values collected in workspaceInfos which in turn
	// is prepared by starting off with unpersisted workspace data and enrich it with termination progress
	// and error details
	result := make([]WorkspaceAgentInfo, 0, len(workspaceInfos))
	for _, agentInfo := range workspaceInfos {
		result = append(result, agentInfo)
	}

	return result
}

func (r *reconciler) collectInfoForUnpersistedWorkspaces(existingWorkspaces []*parsedWorkspace) map[workspaceInfoKey]WorkspaceAgentInfo {
	workspaceAgentInfos := make(map[workspaceInfoKey]WorkspaceAgentInfo)

	for _, workspace := range existingWorkspaces {
		// if Rails already knows about the latest version of the resource, don't send the info again
		if r.stateTracker.isPersisted(workspace.Name, workspace.ResourceVersion) {
			r.log.Debug("Skipping sending workspace info. GitLab already has the latest version", logz.WorkspaceName(workspace.Name))
			continue
		}

		key := workspaceInfoKey{
			Name:      workspace.Name,
			Namespace: workspace.Namespace,
		}
		workspaceAgentInfos[key] = WorkspaceAgentInfo{
			Name:                    workspace.Name,
			Namespace:               workspace.Namespace,
			LatestK8sDeploymentInfo: workspace.K8sDeploymentInfo,
		}
	}

	return workspaceAgentInfos
}

func (r *reconciler) enrichRailsPayloadWithErrorDetails(payload map[workspaceInfoKey]WorkspaceAgentInfo, errorsSnapshot map[errorTrackerKey]operationState) {
	// for each entry in the error snapshot, either a new payload has to be
	// created or the error details must be merged into the existing payload
	// being sent over to rails
	for trackerKey, state := range errorsSnapshot {
		if state.errorDetails == nil {
			// this case will occur when the corresponding operation is still
			// running and the result is not known yet
			continue
		}

		key := workspaceInfoKey{
			Name:      trackerKey.name,
			Namespace: trackerKey.namespace,
		}

		var agentInfo WorkspaceAgentInfo
		var exists bool

		agentInfo, exists = payload[key]
		if !exists {
			agentInfo = WorkspaceAgentInfo{
				Name:      trackerKey.name,
				Namespace: trackerKey.namespace,
			}
		}

		agentInfo.ErrorDetails = state.errorDetails
		payload[key] = agentInfo
	}
}

func (r *reconciler) enrichRailsPayloadWithWorkspaceTerminationProgress(payload map[workspaceInfoKey]WorkspaceAgentInfo, unterminatedWorkspaces map[string]struct{}) {
	// For each workspace that has been scheduled for termination, check if it exists in the cluster
	// If it is missing from the cluster, it can be considered as Terminated otherwise Terminating
	for entry, currentTerminationDetails := range r.terminationTracker {
		lastSavedProgress := currentTerminationDetails.progress
		_, existsInCluster := unterminatedWorkspaces[entry.name]

		if lastSavedProgress == TerminationProgressTerminated && existsInCluster {
			r.log.Warn("Workspace is Terminated in tracker but exists in cluster",
				logz.WorkspaceName(entry.name),
				logz.WorkspaceNamespace(entry.namespace),
			)
		}

		// only update terminationTracker if a workspace was terminating before
		// but no longer exists in the workspace
		if lastSavedProgress == TerminationProgressTerminating && !existsInCluster {
			latestDetails := terminationDetails{
				progress: TerminationProgressTerminated,
				isSynced: false,
			}
			r.terminationTracker.set(entry.name, entry.namespace, latestDetails)
		}
	}

	// enrich payload with termination progress for workspaces that haven't been
	// synced with Rails yet
	for entry, latestTerminationDetails := range r.terminationTracker {
		if latestTerminationDetails.isSynced {
			continue
		}

		key := workspaceInfoKey{
			Name:      entry.name,
			Namespace: entry.namespace,
		}

		var agentInfo WorkspaceAgentInfo
		var exists bool

		agentInfo, exists = payload[key]
		if !exists {
			agentInfo = WorkspaceAgentInfo{
				Name:      entry.name,
				Namespace: entry.namespace,
			}
		}

		agentInfo.TerminationProgress = latestTerminationDetails.progress
		payload[key] = agentInfo
	}
}

func (r *reconciler) performWorkspaceUpdateRequestToRailsApi(
	ctx context.Context,
	updateType WorkspaceUpdateType,
	workspaceAgentInfos []WorkspaceAgentInfo,
) (workspaceRailsInfos []*WorkspaceRailsInfo, retError error) {
	// Do the POST request to the Rails API
	r.log.Debug("Making GitLab request")

	if workspaceAgentInfos == nil {
		// In case there is nothing to report to rails, populate the payload with an empty slice
		// to be explicit about the intent
		// TODO: add a test case - https://gitlab.com/gitlab-org/gitlab/-/issues/407554
		workspaceAgentInfos = []WorkspaceAgentInfo{}
	}

	startTime := time.Now()
	var requestPayload = RequestPayload{
		UpdateType:          updateType,
		WorkspaceAgentInfos: workspaceAgentInfos,
	}
	// below code is from internal/module/starboard_vulnerability/agent/reporter.go
	resp, err := r.api.MakeGitLabRequest(ctx, "/reconcile",
		modagent.WithRequestMethod(http.MethodPost),
		modagent.WithJsonRequestBody(requestPayload),
	) // nolint: govet
	if err != nil {
		return nil, fmt.Errorf("error making api request: %w", err)
	}
	r.log.Debug("Made request to the Rails API",
		logz.StatusCode(resp.StatusCode),
		logz.RequestId(resp.Header.Get(httpz.RequestIdHeader)),
		logz.DurationInMilliseconds(time.Since(startTime)),
	)

	defer errz.SafeClose(resp.Body, &retError)
	if resp.StatusCode != http.StatusCreated {
		_ = ioz.DiscardData(resp.Body)
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %w", err)
	}

	var responsePayload ResponsePayload
	err = json.Unmarshal(body, &responsePayload)
	if err != nil {
		return nil, fmt.Errorf("error parsing response body: %w", err)
	}

	r.log.Debug(
		"Read body from the Rails API",
		logz.PayloadSizeInBytes(len(body)),
		logz.WorkspaceDataCount(len(responsePayload.WorkspaceRailsInfos)),
	)

	return responsePayload.WorkspaceRailsInfos, nil
}

func (r *reconciler) handleDesiredStateIsTerminated(ctx context.Context, name string, namespace string, actualState string) error {
	if actualState == WorkspaceStateTerminated {
		r.log.Debug("ActualState=Terminated has been persisted by Rails and hence deleting it from terminationTracker", logz.WorkspaceNamespace(namespace))
		r.terminationTracker.delete(name, namespace)
		r.stateTracker.delete(name)
		return nil
	}

	namespaceExists, err := r.k8sClient.NamespaceExists(ctx, namespace)
	if err != nil {
		return fmt.Errorf("failed to check if namespace exists: %w", err)
	}

	if !namespaceExists {
		details := terminationDetails{
			progress: TerminationProgressTerminated,
			isSynced: false,
		}
		r.terminationTracker.set(name, namespace, details)
		// nothing to do as the workspace is already absent in the cluster
		return nil
	}

	key := terminationTrackerKey{
		name:      name,
		namespace: namespace,
	}

	details, exists := r.terminationTracker[key]
	if exists {
		// workspace is already being tracked in the terminationTracker so there
		// is no need to try to delete the namespace again
		r.log.Debug("Namespace termination has been attempted before so ignoring this attempt", logz.WorkspaceNamespace(namespace))
		details.isSynced = true
		r.terminationTracker.set(name, namespace, details)

		return nil
	}

	// Since workspace termination has not been attempted before, it will be done now
	// and a corresponding entry will be set in the terminationTracker
	r.log.Debug("Namespace for terminated workspace exists, so deleting the namespace", logz.WorkspaceNamespace(namespace))

	// DeleteNamespace may be called while a delete operation is ongoing for the same workspace
	// This may occur in cases where a partial sync cycle (with termination request) is followed by a full sync
	err = r.k8sClient.DeleteNamespace(ctx, namespace)
	if err != nil {
		return fmt.Errorf("failed to terminate workspace by deleting namespace: %w", err)
	}

	details = terminationDetails{
		progress: TerminationProgressTerminating,
		isSynced: false,
	}
	r.terminationTracker.set(name, namespace, details)

	return nil
}

func (r *reconciler) Stop() {
	// this will be invoked at the end of each full cycle with the outgoing reconciler and its informer being stopped
	// and new reconcilers (with new informers) created from scratch. The underlying principle is to prevent a re-use of
	// reconciler state when a full sync occurs to prevent issues due to corruption of internal state
	// However, this decision can be revisited in the future in of any issues
	// issue: https://gitlab.com/gitlab-org/gitlab/-/issues/404748
	r.informer.Stop()
}
